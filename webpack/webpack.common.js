const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const config = {
  srcFolder: 'src',
  outputFolder: 'public'
};

const cleanDist = new CleanWebpackPlugin(
  [
    `${config.outputFolder}/fonts`,
    `${config.outputFolder}/css`,
    `${config.outputFolder}/img`,
    `${config.outputFolder}/js`,
    `${config.outputFolder}/*.html`
  ],
  {
    root: path.resolve(__dirname, '../')
  }
);

module.exports = {
  entry: {
    main: `./${config.srcFolder}/js/index.js`
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, `../${config.outputFolder}`)
  },
  module: {
    rules: [
      {
        test: /\.js/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              publicPath: path => {
                console.log(path);
                return path.replace(/src/, '..');
              },
              outputPath: path => {
                return path.replace(/src/, '');
              }
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/fonts',
              publicPath: '../fonts'
            }
          }
        ]
      },
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: './[name].html'
            }
          },
          {
            loader: 'string-replace-loader',
            options: {
              search: '../',
              replace: ''
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'html-loader'
          },
          {
            loader: 'pug-html-loader',
            options: {
              pretty: true
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: './[name].html'
            }
          },
          {
            loader: 'string-replace-loader',
            options: {
              search: '../',
              replace: ''
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'html-loader'
          }
        ]
      }
    ]
  },
  plugins: [cleanDist]
};
