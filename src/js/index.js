import '../css/style.scss';
import Icon from '../img/whatsapp-logo.png';

const requireAll = r => r.keys().forEach(r);
requireAll(require.context('../', true, /^.\/[a-z]+\/\w+\.(pug|html)$/));

function component() {
  var element = document.createElement('div');

  element.innerHTML = 'Hello world';
  element.classList.add('hello');

  // Add the image to our existing div.
  var myIcon = new Image();
  myIcon.src = Icon;
  element.appendChild(myIcon);

  return element;
}

document.body.appendChild(component());
