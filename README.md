# Webpack Quick Starter for websites

Build your page structure using PugJS.  
Make it beautiful and write your styles in Sass.  
Handle user actions with the latest Javascript features.  
Make sure your JS is working as intended and test it with Jest.

## Usage

Install dependencies using `yarn` or `npm`

```
yarn OR npm i
```

Run with dev server

```
yarn start OR npm start
```

Run without dev server

```
yarn dev OR npm run dev
```

Build production version

```
yarn build OR npm run build
```

Test Javscript using Jest

```
yarn test OR npm run test
```
